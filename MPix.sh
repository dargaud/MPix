#! /bin/bash
# MPix.sh
# This script displays the mega-pixel count of an image (or images)
# It relies on ImageMagick (or possibly GraphicsMagick) and bc being available
# It can also selectively copy/convert images for a range of values
# (c) Guillaume Dargaud - http://www.gdargaud.net/
# Last modification: 2010/11/25

# Options to pass to the copy command (see 'man cp')
CpOpt="-vi"

# Additional options to pass to ImageMagick convert
ImgOpt="-quality 95"

# You shouldn't have to change anything below here
ShowName=1
ShowCount=1
Min=0
Max=9999999999
Divider="/1024/1024"

usage() {
cat << EOF
USAGE: $0 [Options] File [Files...]
  Gives the pixel count of an image (10 for 10 million pixels).
OPTIONS:	
  -n	Display filenames but not the pixel count (used for selecting images in a range of pixels)
  -x	Display pixel count but not the filenames
  -r	Display 'MPix Filename' instead of 'Filename MPix', allowing you to pipe to 'sort -n'
  -m	Pixel count is in megapixels (default)
  -k	Pixel count is in kilopixels
  -p	Pixel count is in pixels
  -g N	Only display/copy if the pixel count is greater than the value given (inclusive)
  -l N	Only display/copy if the pixel count is lower than the value given (inclusive)
  -c Dir	Copy images that are within requested -g/-l values to a specific directory
  -t Nb	When used in addition to -c, will downsize the image to a given [mega][kilo]pixel count (or simply copied if too small)
EXAMPLES:
  $0 -prg \$((1024*768)) *.jpg | sort -n   to sort images bigger than 1024x768 pixels by number of pixels
  $0 -c /tmp/tv -t 2 -g 10 -l 12 *.png   to copy and convert images in the 10~12 MPix range to 2 MPix in /tmp/tv
EOF
}

while getopts hnxrmkpg:l:c:t: opt; do
  case $opt in
	n) unset ShowCount;;
	x) unset ShowName;;
	r) Reverse=1;;
	m) M=1;;
	k) K=1; Divider="/1024";;
	p) P=1; Divider="";;
	g) Min=$OPTARG;;
	l) Max=$OPTARG;;
	c) Dest=$OPTARG;;
	t) Trans=$OPTARG;;
	h) usage; exit 1;;
    ?) echo "Invalid option: -$OPTARG" >&2; exit 1;;
  esac
done

shift $(( OPTIND - 1 )) 

if [ $# -eq 0 ]; then usage; exit 1; fi

if [ "$ShowName" ] && [ "$ShowCount" ]; then Join=" "; fi

while [ "$1" != "" ]; do
    Count=$(($(identify -format "%w * %h$Divider\\n" "$1")))
    if [ "$Min" -le $Count ] && [ $Count -le "$Max" ]; then 
	# Deals with the displayed line
	if [ $ShowName ]; then Name="$1"; fi
	if [ $ShowCount ]; then Cnt=$Count; fi
	if [ "$ShowName" ] || [ "$ShowCount" ]; then 
	  if [ $Reverse ]; then echo "$Cnt$Join$Name"; else echo "$Name$Join$Cnt"; fi
	fi

	# Deals with file copy / conversion
	if [ "$Dest" ]; then 
	  if [ "$Trans" ]; then	# Convert
		DestFile="$Dest/$(basename "$1")"
                Count=$(($(identify -format "%w * %h" "$1")))
		Mult="1024*1024"	# Default
		if [ $M ]; then Mult="1024*1024"; fi
		if [ $K ]; then Mult="1024"; fi
		if [ $P ]; then Mult="1"; fi
			# bash arithmetics only work with integers, but here we need a sqrt, so we use bc and truncate the result with sed
			RS=$( echo "100 * sqrt ( $Trans * $Mult / $Count ) + 0.5" | bc -l | sed -e "s/\\..*//")
			echo "$RS"
			if [ "$RS" -ge 100 ]; then	# Simple copy
				cp $CpOpt "$1" "$Dest/"; 
			else	# Resampling
				convert "$1" -resize $RS% $ImgOpt "$DestFile"
			fi
		else	# Only copy
			cp $CpOpt "$1" "$Dest/"; 
	  fi
	fi
  fi
  shift		# Next image
done
